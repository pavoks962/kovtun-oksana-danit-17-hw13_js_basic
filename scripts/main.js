// - При запуску програми на екрані має відображатись перша картинка.
// - Через 3 секунди замість неї має бути показано друга картинка.
// - Ще через 3 секунди – третя.
// - Ще через 3 секунди – четверта.
// - Після того, як будуть показані всі картинки - цей цикл має розпочатися наново.
// - Після запуску програми десь на екрані має з'явитись кнопка з написом `Припинити`.
// - Після натискання на кнопку `Припинити` цикл завершується, на екрані залишається показаною та картинка, 
// яка була там при натисканні кнопки.
// - Поруч із кнопкою `Припинити` має бути кнопка `Відновити показ`, при натисканні якої цикл триває з тієї картинки, 
// яка в даний момент показана на екрані.
// - Розмітку можна змінювати, додавати потрібні класи, ID, атрибути, теги. 

// #### Необов'язкове завдання підвищеної складності
// - При запуску програми на екрані повинен бути таймер з секундами та мілісекундами, що показує, скільки залишилося до показу наступної картинки.
// - Робити приховування картинки та показ нової картинки поступовим (анімація fadeOut/fadeIn) протягом 0.5 секунди.


"use strict"

const container = document.querySelector('.container')
const btnStop = document.createElement('button');
btnStop.innerText = 'Припинити';
btnStop.className = 'btn-stop';
container.append(btnStop);

const btnStart = document.createElement('button');
btnStart.innerText = 'Відновити показ';
btnStart.className = 'btn-start';
container.append(btnStart);

const img = document.querySelectorAll('.image-to-show');

let slide = 1;

const hiddenImg = () => {
    for (let i = 0; i < img.length; i++) {
        img[i].classList.add('hidden');
    }
}
const showImg = () => {
    img[slide].classList.remove('hidden');
    slide++;
}
let myInterval = null;
myInterval =
    setInterval(() => {
        hiddenImg()
        showImg()
        if (slide > img.length-1) {
           slide = 0;
        } 
        myInterval
    }, 3000);

btnStart.addEventListener('click', e => {
   myInterval =
    setInterval(() => {
        hiddenImg()
        showImg()
        if (slide > img.length-1) {
           slide = 0;
        } 
        myInterval
    }, 3000);
  console.log(slide)
})
   
btnStop.addEventListener('click', e => {
    clearInterval(myInterval)
    console.log(slide)

})
